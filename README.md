# Text Pattern Matching Artémis COUSSIN

In this project there is a file project_text_pattern_Artemis_COUSSIN.py.
In it, you can call two differents algorithms (Simple Search and Search LCP) to find a pattern in a text.
When you see some propositions (1,2 3 or 4), write the number of the action you want to launch.
1 is to find your pattern with the Simple Search method. 
2 is to find your pattern with the LCP search method.
3 is to change the string and the pattern on which you want to do the searching, the suffix array is automatically print after your choices.
4 is to stop the programm when you're done
I hope it's clear for you, have fun !
